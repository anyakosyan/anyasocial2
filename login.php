<?php @include 'header.php'?>

    <div class="main">
        <section class="sign-in">
            <div class="container">
                <div class="signin-content">
                    <div class="signin-image">
                        <figure><img src="images/signin-image.jpg" alt="sing up image"></figure>
                        <a href="signUp.php" class="signup-image-link">Create an account</a>
                    </div>

                    <div class="signin-form">
                        <h2 class="form-title">Sign in</h2>
                        <form method="POST" class="register-form" id="login-form" action="server.php">
                              <div class="signup_error">
                                    <?php 
                                        if(isset($_SESSION['error-email'])) {
                                            print_r($_SESSION['error-email']);
                                        }
                                    ?>
                                </div>
                            <div class="form-group">
                                <label for="your-email"><i class="zmdi zmdi-account material-icons-name"></i></label>
                                <input type="text" name="email" id="your-email" placeholder="Your Name"/>
                            </div>
                                <div class="signup_error">
                                    <?php 
                                        if(isset($_SESSION['error-password'])) {
                                            print_r($_SESSION['error-password']);
                                        }
                                    ?>
                                </div>
                            <div class="form-group">
                                <label for="your-password"><i class="zmdi zmdi-lock"></i></label>
                                <input type="password" name="password" id="your-password" placeholder="Password"/>
                            </div>
                            <div class="form-group">
                                <input type="checkbox" name="remember-me" id="remember-me" class="agree-term" />
                                <label for="remember-me" class="label-agree-term"><span><span></span></span>Remember me</label>
                            </div>
                            <div class="form-group form-button">
                                <input type="submit" name="signIn" id="signin" class="form-submit signIn" value="Log in"/>
                            </div>
                        </form>
                        <div class="social-login">
                            <span class="social-label">Or login with</span>
                            <ul class="socials">
                                <li><a href="#"><i class="display-flex-center zmdi zmdi-facebook"></i></a></li>
                                <li><a href="#"><i class="display-flex-center zmdi zmdi-twitter"></i></a></li>
                                <li><a href="#"><i class="display-flex-center zmdi zmdi-google"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
 <?php @include 'footer.php'?>