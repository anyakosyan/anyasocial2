/*
Navicat MySQL Data Transfer

Source Server         : armenhomework
Source Server Version : 100406
Source Host           : localhost:3306
Source Database       : user

Target Server Type    : MYSQL
Target Server Version : 100406
File Encoding         : 65001

Date: 2019-10-31 21:00:24
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for comment
-- ----------------------------
DROP TABLE IF EXISTS `comment`;
CREATE TABLE `comment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `text` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `status_id` (`status_id`) USING BTREE,
  KEY `user_id` (`user_id`) USING BTREE,
  CONSTRAINT `comment_ibfk_1` FOREIGN KEY (`status_id`) REFERENCES `status` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `comment_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of comment
-- ----------------------------
INSERT INTO `comment` VALUES ('1', '17', '6', 'sdfawf');
INSERT INTO `comment` VALUES ('2', '18', '6', 'ryseyy');
INSERT INTO `comment` VALUES ('3', '18', '6', 'gfhgfhfgh');
INSERT INTO `comment` VALUES ('4', '17', '6', 'ytuj');
INSERT INTO `comment` VALUES ('5', '17', '6', 'dhdfh');

-- ----------------------------
-- Table structure for friend
-- ----------------------------
DROP TABLE IF EXISTS `friend`;
CREATE TABLE `friend` (
  `user1_id` int(11) NOT NULL,
  `user2_id` int(11) NOT NULL,
  `active` int(255) DEFAULT 0,
  KEY `user1_id` (`user1_id`) USING BTREE,
  KEY `user2_id` (`user2_id`) USING BTREE,
  CONSTRAINT `friend_ibfk_1` FOREIGN KEY (`user1_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `friend_ibfk_2` FOREIGN KEY (`user2_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of friend
-- ----------------------------
INSERT INTO `friend` VALUES ('2', '7', '0');
INSERT INTO `friend` VALUES ('4', '6', '1');
INSERT INTO `friend` VALUES ('3', '2', '0');
INSERT INTO `friend` VALUES ('6', '1', '1');
INSERT INTO `friend` VALUES ('2', '6', '0');
INSERT INTO `friend` VALUES ('2', '8', '0');
INSERT INTO `friend` VALUES ('3', '8', '0');
INSERT INTO `friend` VALUES ('6', '8', '0');

-- ----------------------------
-- Table structure for images
-- ----------------------------
DROP TABLE IF EXISTS `images`;
CREATE TABLE `images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image_name` varchar(255) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `user_id` (`user_id`) USING BTREE,
  CONSTRAINT `images_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=58 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of images
-- ----------------------------
INSERT INTO `images` VALUES ('23', '8.lilit/1572426109award3 - копия.png', '8');
INSERT INTO `images` VALUES ('26', '6.levon/1572426362award3 - копия.png', '6');
INSERT INTO `images` VALUES ('27', '6.levon/1572426365about-wine - копия.png', '6');
INSERT INTO `images` VALUES ('28', '6.levon/1572426386about-us.jpg', '6');
INSERT INTO `images` VALUES ('30', '6.levon/1572426614background1-large.png', '6');
INSERT INTO `images` VALUES ('31', '6.levon/1572426619about-us - копия.jpg', '6');
INSERT INTO `images` VALUES ('39', '6.levon/1572427975Untitled-2.png', '6');
INSERT INTO `images` VALUES ('42', '6.levon/1572428547Untitled-2.png', '6');
INSERT INTO `images` VALUES ('45', '6.levon/1572430464image_upload.png', '6');
INSERT INTO `images` VALUES ('47', '6.levon/1572430525shopping_cart.png', '6');
INSERT INTO `images` VALUES ('48', '6.levon/1572430734Untitled-2.png', '6');
INSERT INTO `images` VALUES ('49', '6.levon/1572432379shopping_cart.png', '6');
INSERT INTO `images` VALUES ('51', '6.levon/1572433036Chesnut-Run-Farm.png', '6');
INSERT INTO `images` VALUES ('52', '6.levon/1572433204alloy.jpg', '6');
INSERT INTO `images` VALUES ('53', '6.levon/15724334902015-LOGO.jpg', '6');
INSERT INTO `images` VALUES ('55', '6.levon/1572434662alloy.jpg', '6');
INSERT INTO `images` VALUES ('56', '8.lilit/1572520269k9basics.jpg', '8');
INSERT INTO `images` VALUES ('57', '8.lilit/1572520274logo.png', '8');

-- ----------------------------
-- Table structure for like
-- ----------------------------
DROP TABLE IF EXISTS `like`;
CREATE TABLE `like` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `status_id` (`status_id`) USING BTREE,
  KEY `user_id` (`user_id`) USING BTREE,
  CONSTRAINT `like_ibfk_1` FOREIGN KEY (`status_id`) REFERENCES `status` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `like_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=63 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of like
-- ----------------------------
INSERT INTO `like` VALUES ('2', '15', '5');
INSERT INTO `like` VALUES ('4', '15', '4');
INSERT INTO `like` VALUES ('6', '17', '1');
INSERT INTO `like` VALUES ('25', '23', '6');
INSERT INTO `like` VALUES ('29', '19', '6');
INSERT INTO `like` VALUES ('62', '17', '6');

-- ----------------------------
-- Table structure for message
-- ----------------------------
DROP TABLE IF EXISTS `message`;
CREATE TABLE `message` (
  `user1_id` int(11) DEFAULT NULL,
  `user2_id` int(11) DEFAULT NULL,
  `text` longtext DEFAULT NULL,
  `date` datetime DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `active` int(11) DEFAULT 1,
  KEY `user1_id` (`user1_id`) USING BTREE,
  KEY `user2_id` (`user2_id`) USING BTREE,
  CONSTRAINT `message_ibfk_1` FOREIGN KEY (`user1_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `message_ibfk_2` FOREIGN KEY (`user2_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of message
-- ----------------------------
INSERT INTO `message` VALUES ('6', '4', 'fghgfh', '2019-10-26 10:48:13', '1');
INSERT INTO `message` VALUES ('6', '4', 'sdfd', '2019-10-26 10:48:13', '1');
INSERT INTO `message` VALUES ('6', '4', 'gjghjh', '2019-10-26 10:48:13', '1');
INSERT INTO `message` VALUES ('6', '4', 'dfgdfgdfg', '2019-10-26 10:48:13', '1');
INSERT INTO `message` VALUES ('6', '4', 'fgfg', '2019-10-26 10:48:13', '1');
INSERT INTO `message` VALUES ('6', '4', 'sdgsdgsdg', '2019-10-26 10:48:13', '1');
INSERT INTO `message` VALUES ('6', '4', 'sdfsdf', '2019-10-26 10:48:13', '1');
INSERT INTO `message` VALUES ('6', '4', 'sdgsdgsdg', '2019-10-26 10:48:13', '1');
INSERT INTO `message` VALUES ('6', '4', 'anya', '2019-10-26 10:48:13', '1');
INSERT INTO `message` VALUES ('6', '4', 'vvcbcvbvc', '2019-10-26 10:48:13', '1');
INSERT INTO `message` VALUES ('4', '6', 'nvbvnbvn', '2019-10-26 10:49:33', '0');
INSERT INTO `message` VALUES ('4', '6', 'gg', '2019-10-26 10:49:33', '0');
INSERT INTO `message` VALUES ('6', '4', 'dgdfgdfgdfg', '2019-10-26 10:48:14', '1');
INSERT INTO `message` VALUES ('4', '6', 'fsdfsdf', '2019-10-26 10:49:33', '0');
INSERT INTO `message` VALUES ('2', '6', 'xdfgxdfgxfg', '2019-10-29 15:36:46', '0');
INSERT INTO `message` VALUES ('2', '6', 'hhhhhh', '2019-10-29 15:36:46', '0');
INSERT INTO `message` VALUES ('2', '6', 'fffffff', '2019-10-31 19:11:47', '0');
INSERT INTO `message` VALUES ('2', '6', 'dddddd', '2019-10-31 19:11:47', '0');

-- ----------------------------
-- Table structure for request
-- ----------------------------
DROP TABLE IF EXISTS `request`;
CREATE TABLE `request` (
  `user1_id` int(11) NOT NULL,
  `user2_id` int(11) NOT NULL,
  PRIMARY KEY (`user1_id`,`user2_id`) USING BTREE,
  KEY `user2_id` (`user2_id`) USING BTREE,
  CONSTRAINT `request_ibfk_1` FOREIGN KEY (`user1_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `request_ibfk_2` FOREIGN KEY (`user2_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of request
-- ----------------------------
INSERT INTO `request` VALUES ('3', '6');
INSERT INTO `request` VALUES ('4', '6');
INSERT INTO `request` VALUES ('6', '5');

-- ----------------------------
-- Table structure for status
-- ----------------------------
DROP TABLE IF EXISTS `status`;
CREATE TABLE `status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `status_img` varchar(255) DEFAULT NULL,
  `status_text` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `user_id` (`user_id`) USING BTREE,
  CONSTRAINT `status_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of status
-- ----------------------------
INSERT INTO `status` VALUES ('14', '6', '6.levon/status/1571428356dog.png', 'dog dog dog');
INSERT INTO `status` VALUES ('15', '6', '6.levon/status/1571428478horse.png', 'horse horse horse');
INSERT INTO `status` VALUES ('16', '7', '6.levon/status/1571428478horse.png', 'lya lya lya');
INSERT INTO `status` VALUES ('17', '4', '6.levon/status/1571428356dog.png', 'fhhfdhh');
INSERT INTO `status` VALUES ('18', '1', '6.levon/status/1571428356dog.png', 'dhshdh');
INSERT INTO `status` VALUES ('19', '4', '6.levon/status/1571428356dog.png', 'dhdfhxfghfh');
INSERT INTO `status` VALUES ('20', '4', '6.levon/status/1571428356dog.png', 'ssssssssss');
INSERT INTO `status` VALUES ('21', '2', ' 6.levon/status/1571428356dog.png', 'xxxxxxxxx');
INSERT INTO `status` VALUES ('22', '2', '6.levon/status/1571428356dog.png', 'ssssssss');
INSERT INTO `status` VALUES ('23', '1', '6.levon/status/1571428356dog.png', 'sssss');
INSERT INTO `status` VALUES ('24', '1', '6.levon/status/1571428356dog.png', 'sssssssssss');
INSERT INTO `status` VALUES ('25', '1', '6.levon/status/1571428356dog.png', 'weqawewqe');
INSERT INTO `status` VALUES ('26', '2', '6.levon/status/1571428356dog.png', 'dggd');
INSERT INTO `status` VALUES ('27', '2', ' 6.levon/status/1571428356dog.png', 'sgsdfgdgdg');
INSERT INTO `status` VALUES ('28', '2', '6.levon/status/1571428356dog.png', 'dgdfg');

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `surname` varchar(255) DEFAULT NULL,
  `age` int(11) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT 'UserImage/avatar.png',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('1', 'anya', 'anyan', '24', 'narek@gmail.com', 'anya12', 'UserImage/avatar.png');
INSERT INTO `user` VALUES ('2', 'anya', 'an', '24', 'artur@gmail.com', 'anya12', 'UserImage/avatar.png');
INSERT INTO `user` VALUES ('3', 'WORDPRESS', 'Aniyan', '24', 'sergey@mail.ru', '$2y$10$rYxyzyhq2mtHxUizOvrLNuXWWDgTgRI.Rfm/LYfc35i/pyuCyvQKm', 'UserImage/avatar.png');
INSERT INTO `user` VALUES ('4', 'armen', 'tatyan', '25', 'admin@gmail.com', '$2y$10$wWJfUSSLYavrcUtYCxB4/uAB.8pLDTsBmJamNZ5GubhNLLCFegmVO', 'UserImage/avatar.png');
INSERT INTO `user` VALUES ('5', 'admin', 'tatyan', '25', 'adminnarek@gmail.com', '$2y$10$qnNR8sv4O80o0CnGZOMHPuhnu8tMP.v2jog5TC8mWMpcIDUqdV9Xy', 'UserImage/avatar.png');
INSERT INTO `user` VALUES ('6', 'levon', 'kosyan', '16', 'levon@mail.ru', '$2y$10$7dRlq/KvQ9bmLWslF6dm4.x6aBGd97iqXeq.OLQoJJaBCaMlOm1dG', '6.levon/1572426614background1-large.png');
INSERT INTO `user` VALUES ('7', 'Vahan', 'Mkrtchyan', '24', 't@mail.ru', '$2y$10$qxNvjd3EY4lx1Up2Q7ldNO/hpvXITvqNe/GNCyf47KcBADbjwHdSi', '6.levon/1570486368WebTime@2x.png');
INSERT INTO `user` VALUES ('8', 'lilit', 'lilyan', '25', 'lilit@mail.ru', '$2y$10$D.p2hx22JAVNyKkFgtuUeOidnhU4.XIbQeLvWxaFKX82YgJZFZ2B6', '8.lilit/1572520274logo.png');
SET FOREIGN_KEY_CHECKS=1;
