<?php @include 'header.php'?>

    <div class="main">

        <!-- Sign up form -->
        <section class="signup">
            <div class="container">
                <div class="signup-content">
                    <div class="signup-form">
                        <h2 class="form-title">Sign up</h2>
                        <form method="POST" class="register-form" id="register-form" action="server.php">
                            <div class="form-group">

                                <div class="signup_error">
                                    <?php 
                                    if(isset($_SESSION['error-name'])){
                                        print($_SESSION['error-name']);
                                    }
                                    ?>  
                                </div>
                                <label for="name"><i class="zmdi zmdi-account material-icons-name"></i></label>
                                <input type="text" name="name" id="name" value="<?php 
                                    if(isset($_SESSION['name'])){
                                        print($_SESSION['name']);
                                    }
                                ?>" placeholder="Your Name"/>
                            </div>
                             <div class="form-group">
                                    <div class="signup_error">
                                    <?php 
                                        if(isset($_SESSION['error-surname'])){
                                            print($_SESSION['error-surname']);
                                        }
                                    ?>
                                </div>
                                <label for="surname"><i class="zmdi zmdi-account material-icons-name"></i></label>
                                <input type="text" name="surname" id="surname" placeholder="Your Surame" value = "<?php 
                                    if(isset($_SESSION['surname'])){
                                        print($_SESSION['surname']);
                                        unset($_SESSION['surname']);
                                    }
                                ?>"/>
                                </div>
                             <div class="form-group">
                                 <div class="signup_error">
                                     <?php
                                        if(isset($_SESSION['error-age'])){
                                            print($_SESSION['error-age']);
                                        }
                                     ?>
                                </div>
                                <label for="age"><i class="zmdi zmdi-calendar-check"></i></label>
                                <input type="number" name="age" id="age" placeholder="Your Age"  value="<?php
                                if(isset($_SESSION['age'])){
                                    print($_SESSION['age']);
                                }
                             ?>"/>
                            </div>
                            <div class="form-group">
                                <div class="signup_error">
                                <?php 
                                    if(isset($_SESSION['error-email'])){
                                        print($_SESSION['error-email']);
                                    }
                                 ?>
                            </div>
                                <label for="email"><i class="zmdi zmdi-email"></i></label>
                                <input type="email" name="email" id="email" placeholder="Your Email"/ value="<?php 
                                if(isset($_SESSION['email'])){
                                    print($_SESSION['email']);
                                }
                             ?>">
                            </div>
                            <div class="form-group">
                                <div class="signup_error">
                                    <?php 
                                        if(isset($_SESSION['error-pass'])) {
                                            print $_SESSION['error-pass'];
                                        }
                                    ?>
                                </div>
                                <label for="password"><i class="zmdi zmdi-lock"></i></label>
                                <input type="password" name="password" id="password" placeholder="Password" value='<?php 
                                    if(isset($_SESSION['pass'])) {
                                        print $_SESSION['pass'];
                                    }
                                ?>'/>
                            </div>
                            <div class="form-group">
                                <div class="signup_error">
                                    <?php 
                                    if(isset($_SESSION['error-confPassword'])) {
                                        print $_SESSION['error-confPassword'];
                                    }
                                ?>
                                </div>
                                <label for="confirmPassword"><i class="zmdi zmdi-lock-outline"></i></label>
                                <input type="password" name="confirmPassword" id="confirmPassword" placeholder="Repeat your password" value='<?php 
                                    if(isset($_SESSION['pass'])) {
                                        print $_SESSION['pass'];
                                    }
                                ?>'/>
                            </div>
                            <div class="form-group">
                                <input type="checkbox" name="agree-term" id="agree-term" class="agree-term" />
                                <label for="agree-term" class="label-agree-term"><span><span></span></span>I agree all statements in  <a href="#" class="term-service">Terms of service</a></label>
                            </div>
                            <div class="form-group form-button">
                                <input type="submit" name="signUp" id="signup" class="form-submit" value="Register"/>
                            </div>
                        </form>
                    </div>
                    <div class="signup-image">
                        <figure><img src="images/signup-image.jpg" alt="sing up image"></figure>
                        <a href="login.php" class="signup-image-link">I am already member</a>
                        <div><a href="login.php" class="signup-image-link">Login</a></div>
                        
                    </div>
                </div>
            </div>
        </section>
    </div>
 <?php @include 'footer.php'?>