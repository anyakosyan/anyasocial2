    <?php 
session_start(); 

if(!isset($_SESSION['user'])){
  header('location:login.php');
}
else{
  $user = $_SESSION['user'];
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Sign Up Form by Colorlib</title>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <!-- Main css -->
    <link rel="stylesheet" href="css/user.css">
    <style>
      
.profile_main .photo-left {
  width: 200px;
  height: 200px;
  position: relative;
  overflow: hidden;
}
.profile_main .user-info_block {
  margin-top: -100px;
}
.profile_main .uploade-image {
  /*opacity: 0;*/
  width: 100%;
}
.profile_main .upload-image_block {
  position: absolute;
  bottom: -60px;
  background-color: rgba(0,0,0,.2);
  transition: bottom .5s ease;
  -moz-transition: bottom .5s ease;
  -webkit-transition: bottom .5s ease;
  -o-transition: bottom .5s ease;
}
.profile_main .photo-left:hover .upload-image_block {
  bottom: 0;
}
.profile_main .upload-image_block .upload-image-text{
  position: absolute;
  left: 0;
  right: 0;
  top: 0;
}
.modal .form-group input {
  display: block;
  padding: 2px 6px;
}
.request {
  position: relative;
  width: 50px;
  height: 50px;
  cursor: pointer;
}
.request_count {
  position: absolute;
  width: 20px;
  height: 20px;
  top: -10px;
  right: 0;
  background-color: blue;
  text-align: center;
  color:white;
  border-radius: 50%;
}
.gallery > div{
  position: relative;
}
.gallery .delete-img, .gallery .profile-img {
  position: absolute;
  right: 13px;
  padding: 4px 8px;
  background-color: #42b1fa;
  color: white;
  border: 0;
}
.gallery .delete-img {
  top: 0;
}
.gallery .profile-img {
  top: 34px;
  padding: 4px 9px;
}
.requestInfo {
  position: absolute;
  background-color: white;
  padding: 20px;
  left: 50%;
  transform: translateX(-50%);
  margin: auto;
  top: 20px;
  width: 300px;
  display: none;
}
main .active-user_block{
  width: 150px;
  height: 100%;
  background-color: #e1e1e1;
}
.active-user_block .active-user {
  padding: 15px 10px;
}
.active-user_block .active-user img{
  width: 20px;
  height: 20px;
  margin-right: 10px;
}
.active-user_block .active-user span {
  display: inline-block;
  width: 6px;
  height: 6px;
  background-color: #42b72a;
  border-radius: 50%;
  margin-left: 10px;
}
    </style>
</head>
<body>
  <header>
    <i class="fa fa-bars" aria-hidden="true"></i>
  </header>
  <main class="profile_main">
    
      <div class="row">
          <div class="left col-lg-4 user-info_block">
              <div class="photo-left">
          <img class="photo" src="<?php
             if(isset($_SESSION['new-img'] )){
            print_r($_SESSION['new-img']);
          }else {
            print_r($user['image']);
          }
           ?>"/>
          <div class="active"></div>
          <div class="upload-image_block">
            <form action="server.php" method="post" enctype='multipart/form-data' class="formProfile">
              <input type="file" name="image" class="uploade-image imageProfile">
              <!-- <button name="uploadImg" class="m-auto">hastatel</button> -->
            </form>
          </div>
<!--            <div class="upload-image_block">-->
<!--                <form method="post" action="server.php" enctype="multipart/form-data">-->
<!--                    <input type="file" name="uploadeImage" class="uploade-image">-->
<!--                </form>-->
<!--            </div>-->
        </div>
        <h4 class="name">
            <span id="name"><?php print_r($user['name']); ?></span>
            <span id="surname"><?php print_r($user['surname']); ?></span>
          </h4>
        <p class="info">UI/UX Designer</p>
        <p class="info" id="email"><?php print_r($user['email']); ?></p>
              <button type="button" id="edit-info" class="btn btn-primary" data-toggle="modal" data-target="#edit_info">poxel tvyalner@</button>
        <div class="stats row">
          <div class="stat col-xs-4" style="padding-right: 50px;">
            <p class="number-stat">3,619</p>
            <p class="desc-stat">Followers</p>
          </div>
          <div class="stat col-xs-4">
            <p class="number-stat">42</p>
            <p class="desc-stat">Following</p>
          </div>
          <div class="stat col-xs-4" style="padding-left: 50px;">
            <p class="number-stat">38</p>
            <p class="desc-stat">Uploads</p>
          </div>
        </div>
        <div class="request">
            <span><i class="fa fa-users" style="font-size: 28px"></i></span>
            <span class="request_count"></span>
            <div class="requestInfo"></div>
          </div>
        <p class="desc">Hi ! My name is Jane Doe. I'm a UI/UX Designer from Paris, in France. I really enjoy photography and mountains.</p>
        <div class="social">
          <i class="fa fa-facebook-square" aria-hidden="true"></i>
          <i class="fa fa-twitter-square" aria-hidden="true"></i>
          <i class="fa fa-pinterest-square" aria-hidden="true"></i>
          <i class="fa fa-tumblr-square" aria-hidden="true"></i>
        </div>
      </div>
      <div class="right col-lg-6">
        <ul class="nav">
          <li>Gallery</li>
<!--          <li>Collections</li>-->
          <li data-toggle="modal" data-target="#statusModal">Add Status</li>
          <li><a href="friends.php">Friends</a></li>
          <li><a href="myStatus.php">My Status</a></li>
          <li><a href="themStatus.php">Them Status</a></li>
          <li><a href="chat.php">Chat</a></li>
        </ul>
         <div id="search" class="input-group search_block" style="max-width: 250px;margin-top: 20px">
            <input type="text" class="form-control" placeholder="Search this blog">
            <div class="search_result"></div>
          </div>
        <span class="follow">Follow</span>
        <div class="row gallery">



          <form action="server.php" method="post" enctype="multipart/form-data" class="addImage-form">
                 <input type="file" name="addImg" class="add-image">
          </form>
        </div>
      </div>
      <div class="col-lg-2 active-user_block align-self-end">
        
      </div>
      </div>
      
  </main>
  <!-- The Modal -->
  <div class="modal" id="edit_info">
      <div class="modal-dialog">
          <div class="modal-content">

              <!-- Modal Header -->
              <div class="modal-header">
                  <h4 class="modal-title">Edit info</h4>
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
              </div>

              <!-- Modal body -->
              <div class="modal-body">
<!--                  <form action="server.php" method="post">-->
                      <div class="form-group"><label for="edit-name">name</label><input type="text" name="name" id="edit-name"></div>
                      <div class="form-group"><label for="edit-surname">surname</label><input type="text" name="surname" id="edit-surname"></div>
                      <div class="form-group"><label for="edit-age">age</label><input type="text" name="email" id="edit-email"></div>
<!--                  </form>-->
              </div>

              <!-- Modal footer -->
              <div class="modal-footer">
                  <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                  <button type="button" class="btn btn-danger" data-dismiss="modal" name="infoSave" id="info_save">Save</button>
              </div>

          </div>
      </div>
  </div>
  <!-- The Modal status-->
  <div class="modal main-modal" id="statusModal">
      <div class="modal-dialog">
          <div class="modal-content">

              <!-- Modal Header -->
              <div class="modal-header">
                  <h4 class="modal-title">Add Status</h4>
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
              </div>

              <!-- Modal body -->
              <div class="modal-body">
                  <form action="server.php" method="post" enctype="multipart/form-data" >
                      <input type="file" name="addStatusImg" class="mb-5">
                      <div class="status-img"></div>
                      <textarea name="addstatusText" id="" cols="30" rows="5"></textarea>
                      <button class="btn btn-danger"  id="save_status" name='shareStatus'>Share</button>
                  </form>

              </div>

              <!-- Modal footer -->

          </div>
      </div>
  </div>

  <!-- amboxj sessian datarkum e  session_destroy() -->
    <!-- JS -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <!-- Popper JS -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  <!-- Latest compiled JavaScript -->
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="js/main.js"></script>
    <script src="js/script.js"></script>
    <script src="js/friend.js"></script>
</body><!-- This templates was made by Colorlib (https://colorlib.com) -->
</html>
