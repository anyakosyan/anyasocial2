<?php 
  session_start();
  if(!isset($_SESSION['user'])) {
      header("Location: login.php");
  }else {
      $friendInfo = $_SESSION['friend-info'];
  }
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>myFriends</title>
	  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <!-- Main css -->
    <link rel="stylesheet" href="css/friend.css">
</head>
<body>
<div class="myFriends_block">
    <h1 class="text-center">My Friends</h1>
    <div class="friends_block">
        <?php foreach ($friendInfo as $key) {?>
                <div class="friend" data-id="<?php print $key['id']?>"><a href="profileFriend.php?id=<?=$key['id']?>"><img src="<?php print $key['image']?>" alt="friendImage" style="width: 40px; height: 40px"> <h5 class="d-inline-block"><?php print ($key['name']." ".$key['surname'])?></h5></a></div>
        <?php } ?>
    </div>
</div>

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <!-- Popper JS -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  <!-- Latest compiled JavaScript -->
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="js/friend.js"></script>
</body>
</html>
