

 <?php
 	session_start(); 
 	include 'database.php';
 	class Controller extends Database {
 		function __construct() {
 			parent::__construct();
		 if($_SERVER['REQUEST_METHOD']=="POST") {
		 	if(isset($_POST['signUp'])) {
		 		$this ->signUp();
		 	}
		 	if(isset($_POST['signIn'])) {
				$this ->signIn();
			}
			if(isset($_FILES['image'])){
			// else if(isset($_POST['uploadImg'])){
//		 		print_r($_POST);
				$this ->profileImg();
			}
			 if(isset($_FILES['addImg'])) {
				$this ->addImg();
			}
			 if (isset($_POST['shareStatus'])){
				$this ->addStatus();
			}
			if(isset($_POST['action'])) {
				if($_POST['action'] == 'search') {
					$this ->result();
					// $val = val;
					// print_r(json_encode($_POST));
				}
				if($_POST['action'] == 'request') {
					$this ->request();
				}
				if($_POST['action'] == 'addImage') {
					header: ('Location: server.php');
					$this ->showImg();
				}
				if($_POST['action'] == 'deleteImg') {
					$this ->deleteImage();
					// header: ('Location:server.php');
					// print_r("lya");
				}
				if($_POST['action'] == 'ProfileImg') {
					$this ->newProfileImg();
				}
				if($_POST['action'] == 'profileImgShow') {
					$this ->profileImgShow();
				}
				if($_POST['action'] == 'requestInfo') {
					$this ->requestInfo();
				}
				if($_POST['action'] == 'acceptRequest') {
					$this ->acceptRequest();
				}
				if($_POST['action'] == 'deleteRequest') {
					$this ->deleteRequest();
				}
				if($_POST['action'] == 'addFriend') {
					$this ->addFriend();
				}
				if($_POST['action'] == 'deleteFriend') {
					$this ->deleteFriend();
				}
				if($_POST['action'] == 'giveBackRequest') {
					$this ->giveBackRequest();
				}
				if ($_POST['action'] == 'friendShow') {
					$this -> friendShow();
				}
				if ($_POST['action'] == 'addMessage') {
					$this -> addMessage();
				}
				if($_POST['action'] == 'allMessage') {
					$this -> allMessage();
				}
				if($_POST['action'] == 'myStatus') {
					$this ->myStatus();
				}
                if($_POST['action'] == 'themStatus') {
                    $this ->themStatus();
                }
                if($_POST['action'] == 'likeStatus'){
                	$this ->likeStatus();
                }
                if($_POST['action'] == 'seeMore'){
                	$this ->seeMore();
                }
                if($_POST['action'] == 'addComment') {
                	$this ->addComment();
                }
                if($_POST['action'] == 'showComment') {
                	$this ->showComment();
                }
                if($_POST['action'] == 'activeMessage') {
                	$this ->activeMessage();
                }
				if($_POST['action'] == 'seeMessage') {
					$this ->seeMessage();
				}
				if($_POST['action'] == 'infoLike') {
					$this ->infoLike();
				}
				if($_POST['action'] == 'seeFriendProfile') {
					$this ->seeFriendProfile();
				}
				if($_POST['action'] == 'deleteFriendTwo') {
					$this ->deleteFriendTwo();
				}
				if($_POST['action'] == 'friendsMyFriend') {
					$this ->friendsMyFriend();
				}
				if($_POST['action'] == 'statusMyFriend') {
					$this ->statusMyFriend();
				}
				if($_POST['action'] == 'likeImage') {
					$this ->likeImage();
				}
				
			}
		
		 }
 		}
 		function signUp() {
			$name = $_POST['name'];
			// die;
		 	$surname = $_POST['surname'];
		 	$age = $_POST['age'];
		 	$email = $_POST['email'];
		 	$password = $_POST['password'];
		 	$confPassword = $_POST['confirmPassword'];
		 	$data = $this ->db ->query("Select * from user where user.email = '$email'") ->fetch_all(true);
 			// print_r($data);die;
		 	if(empty($name) || empty($surname) || empty($age) || empty($email) || empty($password) || 
		 		empty($confPassword) || (!empty($data)) || $password != $confPassword || strlen($password) > 6 || !filter_var($age,FILTER_VALIDATE_INT) || !filter_var($email,FILTER_VALIDATE_EMAIL)) {
		 		if(empty($password)) {
		 			$_SESSION['error-pass']='lracreq passworde';
		 		}else if (strlen($password) > 6){
		 			$_SESSION['error-pass']='nisheri qana@ petq e shat chlini 6ic';
		 		}else if ($password != $confPassword) {
		 			$_SESSION['error-pass']='passwordner@ chen hamnknum';
		 		}else {
		 			$_SESSION['pass']=$password;
		 			
		 		};
		 		if(empty($confPassword)) {
		 			$_SESSION['error-confPassword']='hastatel gaghtnabar@';
		 		}else{
		 			$_SESSION['confPassword']=$confPassword;
		 		}
		 		if(empty($age)) {
		 			$_SESSION['error-age']='lracreq tariq@';
		 		}else if(!filter_var($age,FILTER_VALIDATE_INT)) {
		 			$_SESSION['error-age']='tariq@ petq e lini bnakan tiv';
		 		}else {
		 			$_SESSION['age']=$age;
		 		}
		 		if(empty($email)){
		 			$_SESSION['error-email']='lracreq email';
		 		}else if(!filter_var($email,FILTER_VALIDATE_EMAIL)){
		 			$_SESSION['error-email']='email@ sxal e lracvac';
		 		}else if(!empty($data)){
		 			$_SESSION['error-email']='namn emailov ka grancvac';
		 		}else{
		 			$_SESSION['email']=$email;
		 		}
		 		if(empty($name)) {
		 			$_SESSION['error-name']='lracreq anun@';
		 		}else {
		 			$_SESSION['name']=$name;
		 		}
		 		if(empty($surname)) {
		 			$_SESSION['error-surname']='lracreq azganun@';
		 		}else {
		 			$_SESSION['surname']=$surname;
		 		}
		 		header('Location: signUp.php');
		 	}else {
		 		$password = password_hash($password, PASSWORD_DEFAULT);
		 		$this ->db ->query("Insert into user(name,surname,age,email,password) VALUES('$name', '$surname', $age,'$email', '$password')");
		 		header('Location: login.php');
		 	}
 		}

 		function signIn() {
 			$email = $_POST['email'];
		 	$password = $_POST['password'];
 			$data = $this ->db ->query("Select * from user where user.email = '$email'") ->fetch_all(true);
 			// print_r($data[0]['email']);die;
 			if(empty($email) || empty($password)) {
 				if(empty($email)) {
 					$_SESSION['error-email'] = 'mutqagreq dzer email@';
 				}else if (empty($password)) {
 					$_SESSION['error-password'] = 'mutqagreq dzer password@';
 				}
 				header('Location: login.php');
 			} else if(empty($data)){
 					$_SESSION['error-email'] = 'nman emailov mard chka grancvac';
 					header('Location: login.php');
 				}else if(!password_verify($password, $data[0]['password'])){
 					$_SESSION['error-password'] = 'password@ sxal e';
 					header('Location: login.php');
 				}else {
                    // print_r($data[0]['id']);die;
 					$_SESSION['user']  = $data[0];
 					// $data[0]['active'] = 1;
 					$this ->db ->query("Update user Set active = 1 where email = '$email'");
 					header('Location: profile.php');
 				}
 	}
 	function result() {
 		$data = $_POST['value'];
			$user =  $this ->db->query("Select * from user Where name Like '$data%' or surname Like '$data%'")->fetch_all(true);
//			print_r(json_encode($user));die;
			$id = $_SESSION['user']['id'];
			$arr = [];
			foreach($user as $v){
				$friend = $this ->db ->query("Select * from friend where (user1_id = $id and user2_id = $v[id]) or (user2_id = $id and user1_id = $v[id])")->fetch_all(true);
				if(empty($friend)){
					$v['status'] = 'enker chi';
					// menq $v in nor key enq avelacnum vor jsum heto grenq
					// bayc userum chi avelacnum dra hamar nor $arr[] ksarqenq push kanenq $vn u print kanenq vorjsov heto vercnenq
				}else {
					$v['status'] = 'enker e';
				}

				$request = $this ->db ->query("Select * from request where (user1_id = $id and user2_id = $v[id]) or (user2_id = $id and user1_id = $v[id])")->fetch_all(true);
				  if(!empty($request)){
					if($request[0]['user1_id']==$id){
						$v['status'] = 'es em uxarkel';
					}
					else{
						$v['status'] = 'inqn e uxarkel';

					}
				  }

				  if($v['id']==$id){
						$v['status'] = 'es em';

				  }
			$arr[] = $v;
			}
			print_r(json_encode($arr));
		}
		function profileImg() {
			$img = $_FILES['image'];
			$name = $_SESSION['user']['name'];
			$x = $_SESSION['user']['id'];
			// print_r($img);die;
			$hasce = "$x.$name/".time().$img['name'];
			move_uploaded_file($img['tmp_name'], $hasce);
			$_SESSION['new-img'] = $hasce;
			$this ->db ->query("Update user Set image = '$hasce' Where id = $x");
			$this ->db ->query("Insert into images(image_name,user_id) VALUES('$hasce', $x)");
			header('Location: profile.php');
		}
		function addImg() {
			$addImg = $_FILES['addImg'];
			$x = $_SESSION['user']['id'];
			$name = $_SESSION['user']['name'];
			$image_name = $addImg['name'];
			if(!file_exists("$x.$name")) {
				mkdir("$x.$name");
			}
			$hasce = "$x.$name/".time().$addImg['name'];
			move_uploaded_file($addImg['tmp_name'],$hasce);
			$_SESSION['add-img'] = $hasce;
			$this ->db ->query("Insert into images(image_name,user_id) VALUES('$hasce', $x)");
				header('Location: profile.php');
		}
		function showImg() {
			$x = $_SESSION['user']['id'];
			$img = $this ->db ->query("Select * from images where images.user_id = $x") ->fetch_all(true);
			print_r(json_encode($img));
		}
		function deleteImage() {
			$dataId = $_POST['value'];
			$id = $_SESSION['user']['id'];
            $delImg = $this ->db ->query("Select image_name from images where id = $dataId") ->fetch_all(true);
            $a = explode('/',$delImg[0]["image_name"])[1];
            $profImg = $this ->db ->query("Select image from user where id = $id") ->fetch_all(true);
            $b = explode('/',$profImg[0]['image'])[1];
            $this ->db ->query("Delete from images where id = $dataId");
            unlink($delImg[0]["image_name"]);
            if($a == $b) {
            	$this ->db ->query("Update user Set image = 'UserImage/avatar.png' where id = $id");
            	$_SESSION['user']['image'] = 'UserImage/avatar.png';
            	print_r('UserImage/avatar.png');
            }

		}
		function newProfileImg() {
			$dataId = $_POST['value'];
			$x = $_SESSION['user']['id'];
			$this ->db ->query("Update user Set image=(Select image_name from images where id = $dataId ) where id = $x");
			$image = $this ->db ->query("Select image from user where id = $x") ->fetch_all(true);
			$_SESSION['edit-img'] = $image[0]['image'];
			print_r($_SESSION['edit-img']);
		}
		function profileImgShow() {
			$x = $_SESSION['user']['id'];
			$image = $this ->db ->query("Select image from user where id = $x") ->fetch_all(true);
			print_r($image[0]['image']);
		}
		function request() {
			$x = $_SESSION['user']['id'];
			$requestCount = $this ->db ->query("SELECT COUNT(*) as count FROM request
			where user2_id = $x") ->fetch_all(true);
			print_r($requestCount[0]['count']);

		}
		function requestInfo() {
			$x = $_SESSION['user']['id'];
			$requestCount = $this ->db ->query("Select * from user where id in (Select user1_id from request where user2_id = $x)") ->fetch_all(true);
			print_r(json_encode($requestCount));
		}
		function acceptRequest() {
			$x = $_SESSION['user']['id'];
			$userId = $_POST['value'];
			$this ->db ->query("Delete from request where user1_id = $userId");
			$this ->db ->query("Insert into friend(user1_id,user2_id) VALUES('$userId','$x')");
		}
			function deleteRequest() {
			$x = $_SESSION['user']['id'];
			$userId = $_POST['value'];
			$this ->db ->query("Delete from request where user1_id = $userId");
		}
		function addFriend() {
			$id = $_SESSION['user']['id'];
			$resultId = $_POST['value'];
			print($resultId);
			$this ->db ->query("Insert into request(user1_id,user2_id) VALUES('$id','$resultId')");
		}
		function deleteFriend() {
			$id = $_SESSION['user']['id'];
			$resultId = $_POST['value'];
			print($resultId);
			$this ->db ->query("Delete from friend where (user1_id = $resultId and user2_id = $id) or user2_id = $resultId and user1_id = $id
				");
		}
		function giveBackRequest() {
			$id = $_SESSION['user']['id'];
			$resultId = $_POST['value'];
			print($resultId);
			$this ->db ->query("Delete from request where user1_id = $id and user2_id = $resultId");
		}
		function friendShow() {
			$id = $_SESSION['user']['id'];
			$friendShow = $this ->db ->query("Select * from user Join friend on (id = user1_id and user2_id = $id) or (id = user2_id and user1_id = $id)") ->fetch_all(true);
			print_r(json_encode($friendShow));
			$_SESSION['friend-info'] = $friendShow;
		}
		
		function addMessage() {
			$id = $_SESSION['user']['id'];
			$friendId = $_POST['friendId'];
			$messageText =$_POST['message'];
			$this ->db ->query("Insert into message(user1_id,user2_id,text) VALUES('$id', '$friendId', '$messageText')");
		}
		function allMessage() {
			$id = $_SESSION['user']['id'];
			$friendId = $_POST['friendId'];
			$allMessage = $this ->db ->query("Select * from message where (user1_id = $id and user2_id = $friendId) or (user2_id = $id and user1_id = $friendId)") ->fetch_all(true);
			print_r(json_encode($allMessage));

		}
		function activeMessage(){
			$id = $_SESSION['user']['id'];
			$message = $this ->db ->query("Select * from `message` where user2_id = $id") ->fetch_all(true);
			$a = [];

			foreach ($_SESSION['friend-info'] as $key) {
				$countActiveMessage = [];
				foreach ($message as $item) {
					$countActiveMessage = $this ->db ->query("Select COUNT(*) as count from `message` where (user2_id = $id and user1_id = $key[id]) and active = 1") ->fetch_all(true);
				}
				$key['count'] = $countActiveMessage[0]['count'];
				$a[] = $key;
			}
			print_r(json_encode($a));
		}
		function seeMessage() {
			$id = $_SESSION['user']['id'];
			$data = $_POST['value'];
			$this ->db ->query("Update message Set active = 0 where user1_id = $data and user2_id = $id");
		}
		function addStatus() {
			$id = $_SESSION['user']['id'];
			$img = $_FILES['addStatusImg'];
			$imgName =$img['name'];
			$text = $_POST['addstatusText'];
			$name = $_SESSION['user']['name'];
			if(!file_exists("$id.$name")) {
				mkdir("$id.$name");
				mkdir("$id.$name/"."status");
			}else if(!file_exists("$id.$name/"."status")){
				mkdir("$id.$name/"."status");
			}
			$hasce = "$id.$name".'/status/'.time().$img['name'];
			move_uploaded_file($img['tmp_name'], $hasce);
			$this ->db ->query("Insert into status(user_id,status_img, status_text) VALUES('$id','$hasce', '$text')");
				header("Location: myStatus.php");
		}
		function myStatus() {
            $id = $_SESSION['user']['id'];
            $status = $this ->db ->query("Select * from status where user_id = $id") ->fetch_all(true);
            print_r(json_encode($status));
		}
        function themStatus() {
            $id = $_SESSION['user']['id'];
            $status = $this ->db ->query("Select * from status where user_id in (Select user1_id from friend where user2_id = $id) or user_id in (Select user2_id from friend where user1_id = $id) Limit 10") ->fetch_all(true);
            $a = [];
            foreach ($status as $key) {
            	  $likeCount = $this ->db ->query("Select COUNT(*) as count from `like` where status_id = $key[id]")->fetch_all();
            	  $commentCount = $this ->db ->query("Select COUNT(*) as count from comment where status_id = $key[id]")->fetch_all();
            	  
            	  $key['count'] = $likeCount[0][0];
				  $key['comCount'] = $commentCount[0][0];
            	  $a[]=$key;
            }        
            print_r(json_encode($a));
          
        }
        function infoLike() {
			$id = $_SESSION['user']['id'];
			$data = $_POST['value'];
			$infoLike = $this ->db ->query("Select name,surname,image from user where id in (Select user_id from `like` where status_id = $data)") ->fetch_all(true);
			print_r(json_encode($infoLike));
		}
        function likeStatus(){
        	$id = $_SESSION['user']['id'];
        	$data = $_POST['value'];
        	$likeData = $this ->db ->query("Select * from `like` where user_id = $id and status_id = $data") ->fetch_all(true);
        	// $likeData = json_encode($likeData);
        	// print_r(json_encode($likeData));
        	$a = '';
        	if(empty($likeData)) {
        		$this ->db ->query("Insert into `like`(status_id,user_id) VALUES('$data', '$id')");
        		$a = 'like';
        	}else {
        		$this ->db ->query("Delete from `like` where user_id = $id and status_id = $data");
        		$a = 'dislike';
        	}
        	print_r(json_encode($a));
        }
        function addComment() {
        	$id = $_SESSION['user']['id'];
        	$data = $_POST['value'];
        	$commentText = $_POST['commentText'];
        	$this ->db ->query("Insert into comment(status_id,user_id,text) VALUES('$data','$id','$commentText')");
        }
        function showComment() {
        	$id = $_SESSION['user']['id'];
        	$data =$_POST['value'];
        	$comment = $this ->db ->query("SELECT `user`.`name`,surname,image,`comment`.text FROM `comment`
			JOIN `user` on `comment`.user_id=`user`.id
			WHERE status_id=$data")->fetch_all(true);
			print_r(json_encode($comment));
        }
        function seeMore() {
        	$id = $_SESSION['user']['id'];
        	$count= $_POST['count'];
            $status = $this ->db ->query("Select * from status where user_id in (Select user1_id from friend where user2_id = $id) or user_id in (Select user2_id from friend where user1_id = $id)  Limit $count") ->fetch_all(true);
           $a = [];
            foreach ($status as $key) {
            	  $likeCount = $this ->db ->query("Select COUNT(*) as count from `like` where status_id = $key[id]")->fetch_all(true);
            	    // print_r($likeCount);
            	  $key['count'] = $likeCount;
            	  $a[]=$key;
            }
            print_r(json_encode($a));

        }

        function seeFriendProfile() {
 			$data=$_SESSION['friend-id'];
			$id = $_SESSION['user']['id'];
			$friendInfo = $this ->db ->query("Select * from user where id = $data") ->fetch_all(true);
 			$friendImage = $this ->db ->query("Select * from images where user_id = $data") ->fetch_all(true);
 			$a = [];
			foreach ($friendImage as $key) {
				$likeCount = $this->db ->query("Select COUNT(*) as count from imagelike where image_id = $key[id]") ->fetch_all(true);
				$issetLike = $this ->db ->query("Select id as active from imagelike where image_id = $key[id] and user_id = $id") ->fetch_all(true);
				$key['like_count'] = $likeCount[0]['count'];
				$key['active'] = $issetLike;
				$a[] = $key;
			}
 			$arr=[$friendInfo[0],$a];
 			print_r(json_encode($arr));
		}
		function deleteFriendTwo() {
 			$id = $_SESSION['user']['id'];
 			$data = $_POST['value'];
 			$_SESSION['friend-id'] = $data;
 			$this ->db ->query("Delete from friend where (user1_id = $id and user2_id = $data) or (user1_id = $data and user2_id = $id)");
		}
		function friendsMyFriend() {
			$data = $_SESSION['friend-id'];
			$infoFriend = $this ->db ->query("Select * from `user` where id in(Select user1_id from friend where user2_id = $data) or id in (Select user2_id from friend where user1_id = $data)") ->fetch_all(true);
			print_r(json_encode($infoFriend));
		}
		function statusMyFriend() {
			$data = $_SESSION['friend-id'];
			$statusFriend = $this ->db ->query("Select * from status where user_id in(Select user1_id from friend where user2_id = $data) or id in (Select user2_id from friend where user1_id = $data)") ->fetch_all(true);
			print_r(json_encode($statusFriend));
 		}
 		function likeImage() {
 			$id = $_SESSION['user']['id'];
 			$data = $_POST['value'];
 			$issetLike = $this ->db ->query("Select * from imagelike where image_id = $data and user_id = $id") ->fetch_all(true);
 			$a = '';
 			if(empty($issetLike)) {
				$this ->db ->query("Insert into imagelike(image_id,user_id) VALUES('$data', '$id')");
				$a = 'like';
			}else {
				$this ->db ->query("Delete from imagelike where image_id = $data and user_id = $id");
				$a = 'dislike';
			}
 			print_r(json_encode($a));
		}
 }
	$x = new Controller();
?>



