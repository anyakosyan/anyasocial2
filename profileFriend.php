<?php
session_start();
//print_r($_GET['id']);
 if(!isset($_SESSION['user'])) {
     header("Location: login.php");
 }else {
     $_SESSION['friend-id']  = $_GET['id'];

 }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>friendPage</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="css/user.css">
</head>
<body>
<header>
    <i class="fa fa-bars" aria-hidden="true"></i>
</header>
<main class="profileFriend_main" id="profileFriend_main">

    <div class="row">
        <div class="left col-lg-4 user-info_block">
            <div class="photo-left">
               
            </div>
            <h4 class="name"></h4>
            <p class="info">UI/UX Designer</p>
            <p class="info" id="email"></p>
            <div class="stats row">
                <div class="stat col-xs-4" style="padding-right: 50px;">
                    <p class="number-stat">3,619</p>
                    <p class="desc-stat">Followers</p>
                </div>
                <div class="stat col-xs-4">
                    <p class="number-stat">42</p>
                    <p class="desc-stat">Following</p>
                </div>
                <div class="stat col-xs-4" style="padding-left: 50px;">
                    <p class="number-stat">38</p>
                    <p class="desc-stat">Uploads</p>
                </div>
            </div>
            <p class="desc">Hi ! My name is Jane Doe. I'm a UI/UX Designer from Paris, in France. I really enjoy photography and mountains.</p>
            <div class="social">
                <i class="fa fa-facebook-square" aria-hidden="true"></i>
                <i class="fa fa-twitter-square" aria-hidden="true"></i>
                <i class="fa fa-pinterest-square" aria-hidden="true"></i>
                <i class="fa fa-tumblr-square" aria-hidden="true"></i>
            </div>
        </div>
        <div class="right col-lg-6">
            <ul class="nav">
                <li>Gallery</li>
                <!--          <li>Collections</li>-->
                <li id="her-friend"><a href="friendsMyFriend.php?id=<?=$_GET['id']?>">Friends of </a></li>
                <li id="status-friend"><a href="statusMyFriend.php?id=<?=$_GET['id']?>">Status of </a></li>
                <li><button class="btn deleteFriend" id="deleteFriend"">Delete from Friends</button></li>
            </ul>
            <div class="row gallery">  
               
            </div>
        </div>

        <div class="col-lg-2 active-user_block align-self-end">

        </div>
    </div>

</main>

<!-- amboxj sessian datarkum e  session_destroy() -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<script src="vendor/jquery/jquery.min.js"></script>
<!--<script src="js/main.js"></script>-->
<script src="js/friend.js"></script>
</body>
</html>
