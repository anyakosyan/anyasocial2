$.ajax({
	url: 'server.php',
	type: 'post',
	data: {action: 'friendShow'},
	success: function(r) {
	}
})

// deleteFriend
$('#deleteFriend').on('click',function(){
	var data = $(this).attr('data-id');
	$.ajax({
		url: 'server.php',
		type: 'post',
		data: {action: 'deleteFriendTwo', value: data},
		success: function (r) {
			// console.log(data);
			// console.log(r);
		}
	})
})

	$.ajax({
		url: 'server.php',
		type: 'Post',
		data: {action: 'seeFriendProfile'},
		success: function(r) {
			r = JSON.parse(r);
			console.log(r)
			var img = $(`<img class="photo" src="${r[0].image}"/>`);
			$('#profileFriend_main .photo-left').append(img);
			var fullName = $(`<span>${r[0].name} ${r[0].surname}</span>`);
			$('#profileFriend_main .name').append(fullName);
			$('#profileFriend_main #email').text(r[0].email);
			$('#profileFriend_main #deleteFriend').attr('data-id', r[0].id);
			$('#profileFriend_main #her-friend a').append(r[0].name);
			$('#profileFriend_main #status-friend a').append(r[0].name);
			r[1].forEach((i) => {
				var image = $(`<div class="col-sm-12 col-md-4 images_block">
	                        <div class="image">
	                            <img src="${i.image_name}">
	                        </div>
                     	</div>`);
				if(i.active.length == 0) {
					// $icon = $(``)
					image.append(`<button class="btn like" data-id="${i.id}" id="like_image"><i class='fa fa-thumbs-up' style="margin-right: 6px"></i>Like <span class="like_count">${+i.like_count > 0? i.like_count:''}</span></button>`)
				}else {
					image.append(`<button class="btn like" data-id="${i.id}" id="like_image"><i class='fa fa-thumbs-down' style="margin-right: 6px"></i>Like <span class="like_count">${+i.like_count > 0? i.like_count:''}</span></button>`)
				}
				$('#profileFriend_main .gallery').append(image);

			})
		}
	})

	$.ajax({
		url: 'server.php',
		type: 'Post',
		data: {action: 'friendsMyFriend'},
		success: function(r) {
			r = JSON.parse(r);
			r.forEach(function (i) {
				var div = $(`<div class="friend mb-4"></div><img src="${i.image}" alt="" style="width: 30px; height: 30px;margin-right: 10px;"><h5 class="d-inline-block">${i.name} ${i.surname}</h5></div>`);
				$('#her-friends_block .friend-info').append(div);
			})
		}
	})

	$.ajax({
		url: 'server.php',
		type: 'Post',
		data: {action: 'statusMyFriend'},
		success: function(r) {
			r = JSON.parse(r);
			// console.log(r)
			r.forEach(function (i) {
				console.log(i);
				var div = $(`<div class="status mb-4"><img src="${i.status_img}" alt="" style="max-width:150px"><p>${i.status_text}</p></div>`)
				$('#her-status_block .status_block').append(div);
			})
		}
	})

	$(document).on('click', '.profileFriend_main #like_image', function () {
		var data = $(this).attr('data-id');
		var count = $(this).find('.like_count').text();
		console.log(count);
		$.ajax({
			url: "server.php",
			type: "Post",
			data: {action: "likeImage", value: data},
			success: (r) => {
				r = JSON.parse(r);
				if(r == 'like') {
					count ++;
					if(count != 0) {
						$(this).find('.like_count').text(count);
					}else {
						$(this).find('.like_count').text('');
					}
					$(this).find('i').attr('class', 'fa fa-thumbs-down');
				}else if(r == 'dislike') {
					count --;
					if(count != 0) {
						$(this).find('.like_count').text(count);
					}else {
						$(this).find('.like_count').text('');
					}
					$(this).find('i').attr('class', 'fa fa-thumbs-up');
				}
			}
		})
	})
