	$.ajax({
		url: 'server.php',
		type: 'post',
		data: {action: 'myStatus'},
        success: function(r){
			r = JSON.parse(r);
			console.log(r);
			if(r.length != 0) {
				r.forEach((i) => {
					var div = $(`<div class="status">`+
						`<img class="status-img" src="${i.status_img}">`+
						`<p>${i.status_text}</p>`+
						`<div><button class="btn like" id="status_like"data-id="${i.id}"><i class="fa fa-thumbs-up"></i>Like</button>`+
						`<button class="btn comment">Comment</button>`+
						`</div></div>`);
					$('.myStatus_block').append(div);
				})
			}
		}

	})

	$.ajax({
    url: 'server.php',
    type: 'post',
    data: {action: 'themStatus'},
    success: function(r){
        r = JSON.parse(r);
        console.log(r)
        if(r.length != 0) {
            r.forEach((i) => {
                var div = $(`<div class="status">`+
                    `<img class="status-img" src="${i.status_img}">`+
                    `<p>${i.status_text}</p>`+
                    `<div><button class="btn like" data-id="${i.id}"><i class="fa fa-thumbs-up"></i>Like</button><span class="like-count"> ${+i.count > 0? i.count:''}</span>`+
                    `<button class="btn comment-count" data-id="${i.id}">Comment <span> ${+i.comCount > 0? i.comCount:''}</span></button>`+
                    `<button class="btn comment" data-id="${i.id}">leave a comment</button>`+
                    `</div><div class='comment_block'></div>`+
                    `</div>`);
                $('.themStatus_block').append(div);
            })
            if(r.length >9) {
            	$('.themStatus_block').append(`<button class='' id="see_more">see more </button>`)
            }
        }
    }

})
var n = 10;
$(document).on('click', '#see_more',function(){
	n+=10;
	$.ajax({
		url: 'server.php',
	    type: 'post',
	    data: {action: 'seeMore', count: n},
	    success: function(r) {
	    	r = JSON.parse(r);
	    	console.log(r);
	    	  r.forEach((i) => {
	    	  	console.log(i)
                var div = $(`<div class="status">`+
                    `<img class="status-img" src="${i.status_img}">`+
                    `<p>${i.status_text}</p>`+
                    `<div><button class="btn like" data-id="${i.id}"><i class="fa fa-thumbs-up"></i>Like <span>${+i.count > 0? i.count:''}</span></button>`+
                    `<button class="btn comment-count" data-id="${i.id}">Comment <span> ${+i.comCount > 0? i.comCount:''}</span></button>`+
                    `<button class="btn comment" data-id="${i.id}">leave a comment</button>`+
                    `</div></div>`);
                $('.themStatus_block #see_more').before(div);
            })
    }
	})
})
$(document).on('click', '.like',function(){
	var data = $(this).attr('data-id');
	var count = $(this).parents('.status').find('.like-count').text();
	// console.log(data)
	$.ajax({
		url: 'server.php',
	    type: 'post',
	    data: {action: 'likeStatus', value: data},
	    success: (r) => {
	     r = JSON.parse(r);
	    	console.log(r);
	    	if(r == "like") {
	    		$(this).find('i').removeClass('fa-thumbs-up').addClass('fa-thumbs-down');
	    		count++;
	    	$(this).parents('.status').find('.like-count').text(count);
	    	
	    	}else if(r == "dislike") {
	    		$(this).find('i').removeClass('fa-thumbs-down').addClass('fa-thumbs-up');
	    		count--;
	    			if(count != 0) {
	    			$(this).parents('.status').find('.like-count').text(count);
	    		}else {
	    			$(this).parents('.status').find('.like-count').empty();
	    		}
	    	}
	    }

	})
})
$(document).on('click','.status .like-count', function(){
	var data = $(this).parent('div').find('.like').attr('data-id');
	var num = 0;
	$.ajax({
		url: 'server.php',
		type: 'post',
		data: {action: "infoLike", value: data},
		success: (r) => {
			r = JSON.parse(r);
			console.log(r);
			$(this).parents('.status').find('.show-liker_block').remove();
			var div = $(`<div class="show-liker_block mb-4"></div></br>`);
			$(this).parents('.status').append(div);
			$(this).parents('.status').find('.show-liker').remove();
			r.forEach(i => {
				num++;
				// var div = $(`<div class="show-liker_block mb-4"></div>`);
				var divInner = $(`<div class="show-liker mb-3"><span>${num}. </span><img src="${i.image}" style="width: 30px; height: 30px; margin-right: 10px"><span class="d-inline-block">${i.name} ${i.surname}</span></div>`);;
				$(this).parents('.status').find('.show-liker_block').append(divInner);
			})
		}
	})
})

$(document).on('click', '.comment',function(){
	var data = $(this).attr('data-id');
   if($(this).parents('.status').find('.com').length==1){	
   	$(this).parents('.status').find('.com').remove();
   }else{
   	var textarea = $(`<div class='com'><textarea rows='2' cols='20' class='mt-3'></textarea><button class='d-block add-comment' data-id="${data}">add comment</button></div>`);
	$(this).parents('.status').append(textarea);
}
})
$(document).on('click', '.add-comment',function(){
	var data = $(this).attr('data-id');
	var count = $(this).parents('.status').find('.comment-count span').text();
	if ($(this).parents('.status').find('textarea').val() != ''){
		var commentText = $(this).parents('.status').find('textarea').val();
		$.ajax({
			url: 'server.php',
			type: 'post',
			data: {action: 'addComment', value: data, commentText: commentText},
			success: (r) => {
				count++;
				// console.log(count)
				$(this).parents('.status').find('.comment-count span').text(count);
				$(this).parents('.status').find('textarea').val('');
			}
		})}
	console.log(commentText);
})

$(document).on('click','.comment-count',function(){
	var data = $(this).attr('data-id');
	$.ajax({
		url:'server.php',
		type: 'post',
		data: {action: 'showComment', value: data},
		success: (r) => {
			r = JSON.parse(r);
			console.log(r);
			$(this).parents('.status').find('.infoCommenter').remove();
			var div = $(`<div class="infoCommenter"></div></br>`);
			$(this).parents('.status').append(div);
			$(this).parents('.status').find('.infoCom ').remove();
			r.forEach((i) => {
				var divInner = $(`<div class='infoCom mt-4'><img src="${i.image}" style="width: 30px;height: 30px; margin-right: 20px"><span>${i.name}</span><span> ${i.surname}</span><p>${i.text}</p></div>`)
				$(this).parents('.status').find('.infoCommenter').append(divInner);
			})
		}
	})
})


